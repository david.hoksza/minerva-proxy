const fs = require('fs');
let config = {};

config.allowedInstances = [
    'http://pg-sandbox.uni.lu/minerva',
    'https://minerva-dev.lcsb.uni.lu'
];

config.httpsOptions = {
    pfx: fs.readFileSync('test_cert.pfx'),
    passphrase: 'sample',
    rejectUnauthorized:false
};

config.portToListen = 8080;

config.useHttps = false;

config.CORS = true;

module.exports = config;
